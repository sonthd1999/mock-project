interface Article {
  title: string;
  slug: string;
  body: string;
  createdAt: string;
  updatedAt: string;
  tagList: [];
  description: string;
  author: {
    username: string;
    bio: null;
    image: string;
    following: false;
  };
  favorited: false;
  favoritesCount: 0;
}
