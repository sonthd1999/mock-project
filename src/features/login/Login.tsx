import React from "react";
import { Container } from "react-bootstrap";
import FormLogin from "./FormLogin";

export const Login = () => {
 

  return (
    <Container>
      <h1 className="m-5">
        <i className="fas fa-lock" /> Login
      </h1>
      <FormLogin />
    </Container>
  );
};
