import React, { useContext, useState } from "react";
import { Button, Container } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import { Formik, ErrorMessage } from "formik";
import * as Yup from "yup";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

function FormLogin() {
  const history = useHistory();

  const login = useSelector((state: any) => state.login);
  const dispatch = useDispatch();

  const [initialValue] = useState({
    email: "",
    password: "",
  });

  const loginApp = async (values: Login) => {
    await dispatch(login(values));
    history.push("/home");
    window.location.reload();
  };

  return (
    <div className="mt-4">
      <Formik
        enableReinitialize
        initialValues={initialValue}
        onSubmit={(values: Login) => loginApp(values)}
        validateOnChange={true}
        validateOnBlur={true}
        validationSchema={Yup.object().shape({
          email: Yup.string().email().required(),
          password: Yup.string().required(),
        })}
      >
        {(props) => {
          const {
            values,
            touched,
            errors,
            isSubmitting,
            handleBlur,
            handleChange,
            handleSubmit,
          } = props;
          return (
            <Form className="mx-3 w-50" onSubmit={handleSubmit}>
              <div className="pt-2">
                <Form.Group className="mb-3">
                  <Form.Label>Email</Form.Label>
                  <Form.Control
                    id="email"
                    placeholder="Enter your email"
                    type="text"
                    value={values.email}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    className={
                      errors.email && touched.email
                        ? "text-input error"
                        : "text-input"
                    }
                  />

                  <div className="input-feedback text-change">
                    <ErrorMessage name="email" />
                  </div>
                </Form.Group>

                <Form.Group className="mb-3">
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    id="password"
                    placeholder="Enter your password"
                    type="password"
                    value={values.password}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    className={
                      errors.password && touched.password
                        ? "text-input error"
                        : "text-input"
                    }
                  />
                  <div className="input-feedback text-change">
                    <ErrorMessage name="password" />
                  </div>
                </Form.Group>
              </div>
              <div>
                <Button
                  className="btn-success my-1"
                  type="submit"
                  disabled={isSubmitting}
                >
                  Login
                </Button>
              </div>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
}

export default FormLogin;
