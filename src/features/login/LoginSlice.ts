import { createSlice } from "@reduxjs/toolkit";
import { v4 as uuidv4 } from "uuid";

const initialState: Login = {
  email: "",
  password: "",
};

export const LoginSlice = createSlice({
  name: "login",
  initialState: initialState,
  reducers: {
    loginApp: (state: any, action: any) => {
      const { email, password } = action.payload;
      state.push({ email, password });
    },
    logoutApp: (state: any, action: any) => {
      state.splice(action.payload, 1);
    },
  },
});

const { actions, reducer } = LoginSlice;
export const { loginApp, logoutApp } = actions;
export default reducer;
