import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import articleApi from "../../api/articleApi";

interface InitialState {
  articleList: any;
  articleListBySlug: {};
  isLoadingArticle: boolean;
}

const initialState: InitialState = {
  articleList: [],
  articleListBySlug: {},
  isLoadingArticle: false,
};

export const getArticles = createAsyncThunk(
  "articles/getArticles",
  async () => {
    const res = await articleApi.getArticle();
    return res.articles;
  }
);

export const getArticlesByTags = createAsyncThunk(
  "articles/getArticlesByTags",
  async (nameTag: string) => {
    const res = await articleApi.getArticleByTags(nameTag);
    return res.articles;
  }
);

export const getArticlesBySlug = createAsyncThunk(
  "articles/getArticlesBySlug",
  async (slug: string) => {
    const res = await articleApi.getArticleBySlug(slug);
    // console.log(res.article);
    return res.article;
  }
);

export const getArticlesByAuthor = createAsyncThunk(
  "articles/getArticlesByAuthor",
  async (author: string) => {
    const res = await articleApi.getArticleByAuthor(author);
    console.log(res.articles);
    return res.articles;
  }
);

export const addArticles = createAsyncThunk(
  "articles/addArticles",
  async (value: Article[]) => {
    const res = await articleApi.addArticle(value);
    return res.articles;
  }
);

export const updateArticles = createAsyncThunk(
  "articles/updateArticles",
  async (value: Article[]) => {
    const res = await articleApi.updateArticle(value);
    return res.articles;
  }
);

export const deleteArticles = createAsyncThunk(
  "articles/deleteArticles",
  async (slug: string) => await articleApi.deleteArticle(slug)
);

export const articleSlice = createSlice({
  name: "articles",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      // get list articles
      .addCase(getArticles.pending, (state) => {
        state.isLoadingArticle = true;
      })
      .addCase(getArticles.fulfilled, (state, action) => {
        state.isLoadingArticle = false;
        state.articleList = action.payload;
      })
      .addCase(getArticles.rejected, (state) => {
        state.isLoadingArticle = true;
      })

      // get list article by tag
      .addCase(getArticlesByTags.fulfilled, (state, action) => {
        state.isLoadingArticle = false;
        state.articleList = action.payload;
      })

      // get list article by slug
      .addCase(getArticlesBySlug.fulfilled, (state, action) => {
        state.articleListBySlug = action.payload;
      })

      // get list article by author
      .addCase(getArticlesByAuthor.fulfilled, (state, action) => {
        state.articleList = action.payload;
      })

      // add article
      .addCase(addArticles.fulfilled, (state, action) => {
        state.articleList = action.payload;
      })

      // update article
      .addCase(updateArticles.fulfilled, (state, action) => {
        state.articleList = action.payload;
      });
  },
});

export default articleSlice;
