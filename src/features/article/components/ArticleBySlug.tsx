import React, { useEffect } from "react";
import { Image, Dropdown } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { deleteArticles, getArticlesBySlug } from "../articleSlice";

type Params = {
  slug: string;
};

function ArticleBySlug() {
  const history = useHistory();
  const articleListBySlug = useSelector(
    (state: any) => state.articles.articleListBySlug
  );

  const params = useParams<Params>();
  const slug = params.slug;

  const dispatch = useDispatch();

  const updateArticle = () => {
    history.push(`/editor/${slug}`);
  };

  const deleteArticle = async () => {
    await dispatch(deleteArticles(slug));
    history.push("/");
  };

  useEffect(() => {
    dispatch(getArticlesBySlug(slug));
  }, []);

  return (
    <div className="bg-light py-5 d-flex flex-column align-items-center">
      <div className="d-flex">
        <Image
          src={articleListBySlug.author?.image}
          width="50"
          height="50"
          roundedCircle
        />
        <div className="mx-3 my-1">
          <h6 className="my-0">{articleListBySlug.author?.username}</h6>
          <small>{articleListBySlug.updatedAt}</small>
        </div>
        <Dropdown className="mt-1 mx-3">
          <Dropdown.Toggle variant="secondary">Editor</Dropdown.Toggle>
          <Dropdown.Menu variant="dark">
            <Dropdown.Item onClick={updateArticle}>Edit Article</Dropdown.Item>
            <Dropdown.Item onClick={deleteArticle}>
              Delete Article
            </Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </div>
      <div className="pt-5 pb-3">
        <h2 className="mx-auto h2">{articleListBySlug.title}</h2>
        <p className="mx-auto mb-4 lead">{articleListBySlug.body}</p>
      </div>
    </div>
  );
}

export default ArticleBySlug;
