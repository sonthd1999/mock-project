import React, { useEffect, useState } from "react";
import { Col, Row, Image } from "react-bootstrap";
import ReactPaginate from "react-paginate";
import { NavLink } from "react-router-dom";

interface Props {
  articleList: [];
}

function Article({ articleList }: Props) {
  const [page, setPage] = useState(0);
  const [numItem] = useState(3);
  const [numPage, setNumPage] = useState(0);

  useEffect(() => {
    setNumPage(Math.ceil(articleList?.length / numItem));
  }, [page, numItem]);

  // pagination
  const handlePageClick = (e: any) => {
    let currentPage = e.selected;
    setPage(currentPage);
  };

  let articles = articleList?.slice(page * numItem, (page + 1) * numItem);

  return (
    <div className="mt-4">
      {articles?.map((articles: Article) => (
        <div className="list-group my-3">
          <NavLink
            to={`/articles/${articles.slug}`}
            className="list-group-item list-group-item-action"
          >
            <div className="d-flex w-100 justify-content-between">
              <Row className="mb-1">
                <Col lg={3}>
                  <Image
                    src={articles.author.image}
                    width="50"
                    height="50"
                    roundedCircle
                  />
                </Col>
                <Col className="d-flex flex-column">
                  <h6>{articles.author.username}</h6>
                  <small>{articles.updatedAt}</small>
                </Col>
              </Row>
              <div className="">
                <i className="text-muted far fa-heart" />{" "}
                {articles.favoritesCount}
              </div>
            </div>
            <h3 className="mb-1">{articles.title}</h3>
            <p className="mb-1">{articles.body}</p>
            <small className="text-muted">{articles.description}</small>
          </NavLink>
        </div>
      ))}
      <ReactPaginate
        previousLabel={"Previous"}
        nextLabel={"Next"}
        pageCount={numPage}
        marginPagesDisplayed={1}
        pageRangeDisplayed={2}
        onPageChange={handlePageClick}
        containerClassName={numPage > 1 ? "pagination" : "d-none"}
        activeClassName={"active"}
      />
    </div>
  );
}

export default Article;
