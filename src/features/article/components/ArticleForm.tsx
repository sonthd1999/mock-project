import React, { useEffect, useState } from "react";
import { Button, Col, Row, Container } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import { Formik, FieldArray, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { useSelector, useDispatch } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { addArticles, updateArticles } from "../articleSlice";

type Params = {
  slug: any;
};

function ArticleForm() {
  const params = useParams<Params>();
  const slug = params.slug;

  const articleListBySlug = useSelector(
    (state: any) => state.articles.articleListBySlug
  );

  const history = useHistory();

  const goBackHome = () => history.push("/");
  const dispatch = useDispatch();

  const [initialValue, setInitialValue] = useState({
    title: "",
    description: "",
    body: "",
    tagList: [],
  });

  useEffect(() => {
    slug
      ? setInitialValue(articleListBySlug)
      : setInitialValue({
          title: "",
          description: "",
          body: "",
          tagList: [],
        });
  }, []);

  const handleAddArticle = async (value: Article[]) => {
    await dispatch(addArticles(value));
    goBackHome();
  };

  const handleUpdateArticle = async (value: Article[]) => {
    await dispatch(updateArticles(value));
    goBackHome();
  };

  return (
    <>
      <Container>
        <h1 className="m-5">
          <i className="fas fa-lock" /> Add Article
        </h1>
        <Formik
          enableReinitialize
          initialValues={initialValue}
          onSubmit={(values: any) => {
            slug ? handleUpdateArticle(values) : handleAddArticle(values);
          }}
          validateOnChange={true}
          validateOnBlur={true}
          validateSchema={Yup.object().shape({
            title: Yup.string().required(),
            description: Yup.string().required(),
            body: Yup.string().required(),
          })}
        >
          {(props) => {
            const {
              values,
              touched,
              errors,
              isSubmitting,
              handleBlur,
              handleChange,
              handleSubmit,
            } = props;
            return (
              <Form className="mx-3 w-50" onSubmit={handleSubmit}>
                <div>
                  <Button
                    className="btn-success my-1"
                    type="submit"
                    disabled={isSubmitting}
                  >
                    Publish Article
                  </Button>
                  <Button
                    type="button"
                    variant="outline-secondary"
                    className="mx-2"
                    onClick={goBackHome}
                  >
                    Cancel
                  </Button>
                </div>
                <div className="pt-2">
                  <Form.Group className="mb-3">
                    <Form.Label>Title</Form.Label>
                    <Form.Control
                      id="title"
                      placeholder="Article Title"
                      type="text"
                      value={values.title}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className={
                        errors.title && touched.title
                          ? "text-input error"
                          : "text-input"
                      }
                    />
                    <div className="input-feedback text-change">
                      <ErrorMessage name="title" />
                    </div>
                  </Form.Group>
                  <Form.Group className="mb-3">
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                      id="description"
                      placeholder="What's this article about?"
                      type="text"
                      value={values.description}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className={
                        errors.description && touched.description
                          ? "text-input error"
                          : "text-input"
                      }
                    />
                    <div className="input-feedback text-change">
                      <ErrorMessage name="description" />
                    </div>
                  </Form.Group>
                  <Form.Group className="mb-3">
                    <Form.Label>Body</Form.Label>
                    <Form.Control
                      id="body"
                      as="textarea"
                      rows={3}
                      placeholder="Write your article (in markdown)"
                      type="text"
                      value={values.body}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className={
                        errors.body && touched.body
                          ? "text-input error"
                          : "text-input"
                      }
                    />
                    <div className="input-feedback text-change">
                      <ErrorMessage name="body" />
                    </div>
                  </Form.Group>
                  <FieldArray
                    name="tagList"
                    render={(arrayHelpers) => (
                      <div>
                        <div className="">
                          {values.tagList.map(
                            (tagLists: any, index: number) => (
                              <div>
                                <Row className="my-3" key={index}>
                                  <Col>
                                    <Field
                                      className="form-control"
                                      placeholder="Enter tags"
                                      name={`tagList.${index}`}
                                    />
                                  </Col>
                                  <Col xs={2}>
                                    <Button
                                      variant="danger"
                                      type="button"
                                      onClick={() => arrayHelpers.remove(index)}
                                    >
                                      <i className="far fa-trash-alt"></i>
                                    </Button>
                                  </Col>
                                </Row>
                              </div>
                            )
                          )}
                        </div>
                        <div>
                          <Button
                            onClick={() => arrayHelpers.push("")}
                            className="btn-success"
                            type="button"
                            disabled={isSubmitting}
                          >
                            Add Tag
                          </Button>
                        </div>
                      </div>
                    )}
                  />
                </div>
              </Form>
            );
          }}
        </Formik>
      </Container>
    </>
  );
}

export default ArticleForm;
