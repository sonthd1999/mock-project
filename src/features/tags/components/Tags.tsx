import React from "react";
import { Badge, Container } from "react-bootstrap";
import { useDispatch } from "react-redux";
import { getArticlesByTags } from "../../article/articleSlice";

interface Props {
  tagsList: any;
}

export const Tags = ({ tagsList }: Props) => {
  const dispatch = useDispatch();

  const listArticleByTags = (tags: string) => {
    dispatch(getArticlesByTags(tags));
  };

  return (
    <Container>
      <h5 className="mb-1">Popular HashTags</h5>
      {tagsList.map((tags: any) => (
        <Badge
          pill
          text="dark"
          className="btn btn-light"
          onClick={() => listArticleByTags(tags)}
        >
          {tags}
        </Badge>
      ))}
    </Container>
  );
};
