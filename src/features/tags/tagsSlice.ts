import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import tagsApi from "../../api/tagsApi";

interface InitialState {
  tagsList: any;
  isLoadingTags: boolean;
}

const initialState: InitialState = {
  tagsList: [],
  isLoadingTags: false,
};

export const getTags = createAsyncThunk("tags/getTags", async () => {
  const res = await tagsApi.getTags();
  return res;
});

export const tagsSlice = createSlice({
  name: "tags",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getTags.pending, (state) => {
        state.isLoadingTags = true;
      })
      .addCase(getTags.fulfilled, (state, action) => {
        state.tagsList = action.payload.tags;
        state.isLoadingTags = false;
      })
      .addCase(getTags.rejected, (state) => {
        state.isLoadingTags = true;
      });
  },
});

export default tagsSlice;
