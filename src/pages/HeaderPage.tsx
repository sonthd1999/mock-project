import React from "react";
import { Navbar, Nav, Container } from "react-bootstrap";
import { NavLink } from "react-router-dom";

export const HeaderPage = () => {
  return (
    <>
      <Navbar expand="lg" bg="dark" variant="dark">
        <Container>
          <Navbar.Brand href="#home">sonson</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="me-auto"></Nav>
            <Nav>
              <Nav.Link>
                <NavLink
                  to="/"
                  className="mt-2 mx-3 text-decoration-none text-light"
                  activeClassName="active"
                >
                  Home
                </NavLink>
              </Nav.Link>
              <Nav.Link>
                <NavLink
                  to="/editor"
                  className="mt-2 mx-3 text-decoration-none text-light"
                  activeClassName="active"
                >
                  <i className="far fa-file-alt" /> New Post
                </NavLink>
              </Nav.Link>
              <Nav.Link>
                <NavLink
                  to="/profile"
                  className="mt-2 mx-3 text-decoration-none text-light"
                  activeClassName="active"
                >
                  <i className="fas fa-user-circle" /> Your Profile
                </NavLink>
              </Nav.Link>
              <Nav.Link>
                <NavLink
                  to="/login"
                  className="mt-2 mx-3 text-decoration-none text-light"
                  activeClassName="active"
                >
                  Login
                </NavLink>
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
};
