import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import ArticleBySlug from "../features/article/components/ArticleBySlug";
import ArticleForm from "../features/article/components/ArticleForm";
import { Login } from "../features/login/Login";
import ArticlePage from "./ArticlePage";
import { HeaderPage } from "./HeaderPage";

export const HomePage = () => {
  return (
    <>
      <HeaderPage />
      <Switch>
        <Route path="/articles/:slug" component={ArticleBySlug} />
        <Route path="/editor/:slug" component={ArticleForm} />
        <Route path="/editor" component={ArticleForm} />
        <Route path="/login" component={Login} />
        <Route path="/" component={ArticlePage} />
      </Switch>
    </>
  );
};
