import React, { useEffect } from "react";
import { Col, Container, Nav, Row } from "react-bootstrap";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import {
  getArticles,
  getArticlesByAuthor,
} from "../features/article/articleSlice";
import Article from "../features/article/components/Article";
import { Tags } from "../features/tags/components/Tags";
import { getTags } from "../features/tags/tagsSlice";

const ArticlePage = () => {
  const articleList = useSelector((state: any) => state.articles.articleList);
  const isLoadingArticle = useSelector(
    (state: any) => state.articles.isLoadingArticle
  );
  const tagsList = useSelector((state: any) => state.tags.tagsList);
  const isLoadingTags = useSelector((state: any) => state.tags.isLoadingTags);
  const dispatch = useDispatch();

  const getListArticlesByAuthor = () => {
    dispatch(getArticlesByAuthor("tulinh"));
  };

  const getListArticlesGlobal = () => {
    dispatch(getArticles());
  };

  useEffect(() => {
    dispatch(getArticles());
    dispatch(getTags());
  }, []);

  return (
    <Container>
      <div>
        <Nav variant="tabs" className="my-5">
          <Nav.Item>
            <Nav.Link onClick={getListArticlesGlobal}>Global Feed</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link onClick={getListArticlesByAuthor}>Your Feed</Nav.Link>
          </Nav.Item>
        </Nav>
        <Row>
          <Col lg={7}>
            {isLoadingArticle ? (
              <h1>loading...</h1>
            ) : (
              <Article articleList={articleList} />
            )}
          </Col>
          <Col>
            {isLoadingTags ? (
              <h3>Loading Article...</h3>
            ) : (
              <Tags tagsList={tagsList} />
            )}
          </Col>
        </Row>
      </div>
    </Container>
  );
};

export default ArticlePage;
