import axios, { AxiosRequestConfig, AxiosResponse } from "axios";

export const TOKEN =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxMjkwMzk0MTk0NzMwNDQyNDI2ZjU4YSIsInVzZXJuYW1lIjoidHVsaW5oIiwiZXhwIjoxNjM1MjYxODQ0LCJpYXQiOjE2MzAwNzc4NDR9.9b0j1BLyTLz3cOvKJls5cGKuAYt4GlHNEJKPk16rdMg";

axios.defaults.headers.common["Authorization"] = `Bearer ${TOKEN}`;
axios.defaults.headers.post["Content-Type"] = "application/json";

const axiosClient = axios.create({
  baseURL: "http://localhost:3000/api/",
});

axiosClient.interceptors.request.use(
  function (config: AxiosRequestConfig) {
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

axiosClient.interceptors.response.use(
  function (response: AxiosResponse) {
    return response.data;
  },
  function (error) {
    return Promise.reject(error);
  }
);

export default axiosClient;
