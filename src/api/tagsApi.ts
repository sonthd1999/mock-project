import axiosClient from "./axiosClient";

const tagsApi = {
  getTags: (): Promise<any> => {
    const listTags = axiosClient.get("tags");
    // console.log(listTags);
    return listTags;
  },
};

export default tagsApi;
