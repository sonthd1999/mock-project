import axiosClient from "./axiosClient";

const articleApi = {
  getArticle: (): Promise<any> => {
    const listArticle = axiosClient.get("articles");
    // console.log(listArticle);
    return listArticle;
  },

  getArticleByTags: (nameTag: string): Promise<any> => {
    const listArticleByTags = axiosClient.get("articles", {
      params: {
        tag: nameTag,
      },
    });
    // console.log(listArticleByTags);
    return listArticleByTags;
  },

  getArticleBySlug: (slug: string): Promise<any> => {
    const articleBySlug = axiosClient.get(`articles/${slug}`);
    // console.log(articleBySlug);
    return articleBySlug;
  },

  getArticleByAuthor: (author: string): Promise<any> => {
    const articleByAuthor = axiosClient.get(`articles?author=${author}`);
    console.log(articleByAuthor);
    return articleByAuthor;
  },

  addArticle: (value: Article[]): Promise<any> => {
    const newArticle = axiosClient.post("articles", { article: value });
    // console.log(newArticle);
    return newArticle;
  },

  updateArticle: (value: any): Promise<any> => {
    const { slug, ...values } = value;
    const newArticle = axiosClient.put(`articles/${slug}`, { article: values });
    // console.log(newArticle);
    return newArticle;
  },

  deleteArticle: (slug: string): Promise<any> => {
    return axiosClient.delete(`articles/${slug}`);
  },
};

export default articleApi;
