import { tagsSlice } from "./../features/tags/tagsSlice";
import { articleSlice } from "./../features/article/articleSlice";
import { configureStore } from "@reduxjs/toolkit";

const store = configureStore({
  reducer: {
    articles: articleSlice.reducer,
    tags: tagsSlice.reducer,
  },
});
export default store;
